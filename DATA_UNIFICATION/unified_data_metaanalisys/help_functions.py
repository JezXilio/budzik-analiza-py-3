import pandas as pd
import pandas
import numpy as np

crs_convertion_dict = {'CONTROL ADULT': 5,
                       'CONTROL CHILD': 5,
                       'EMCS': 4.0,
                       'MCS+/EMCS': 3.5,
                       'MCS+': 3.0,
                       'MCS-': 2.0,
                       'MCS-/UWS': 1.5,
                       'UNKNOWN': None,
                       'UWS': 1.0}


def read_database(data_path):
    dataframe = pd.DataFrame.from_csv(data_path)
    columns_to_drop = ['measurement_type', 'measurement_date', 'files_paths', 'fif_file_paths', 'person_id']
    for column in columns_to_drop:
        dataframe.drop(column, axis=1, inplace=True)

    # convert CRS
    dataframe['diag'] = dataframe['diag'].apply(lambda x: crs_convertion_dict[x])
    return dataframe

def merge_measurements_records(frame, debug=False):
    """Merges the same measuerments split into different tours"""
    frame = frame.copy()
    frame['measurement_date'] = pandas.to_datetime(frame.measurement_date)

    max_time_delta = 90

    columns = frame.columns
    record_lists_for_compressing = []

    non_features = set(['person_id', 'diag', 'measurement_type', 'measurement_date',
                        'files_paths', 'fif_file_paths', 'skull']) # skull is nonchanging feature
    feature_columns = set(columns) - non_features

    for person in set(frame.person_id):
        subframe = frame[frame.person_id==person]
        last = None

        # per merge
        to_compress = []
        used_columns = set()
        if debug:
            print("MERGING", person)

        for row_id, row in subframe.sort_values('measurement_date', ascending=True).iterrows():
            if last is None:
                last = row
                to_compress.append(row)
                continue
            if last.diag == row.diag:
                non_empty_columns = []
                for column in feature_columns:
                    if np.isfinite(row[column]):
                        non_empty_columns.append(column)

                overlap = len(used_columns.intersection(non_empty_columns)) > 0
                if not overlap:
                    to_compress.append(row)
                    used_columns.update(non_empty_columns)
                    last = row
                else:
                    if debug:
                        print("Records repeat, new merge, non_empty_columns:", non_empty_columns, "used columns intersection:",
                              used_columns.intersection(non_empty_columns))
                        print("Connected ", len(to_compress), "records")
                    record_lists_for_compressing.append(to_compress)
                    to_compress = [row, ]
                    used_columns = set(non_empty_columns)
                    last = row

            else:
                if debug:
                    print("Connected ", len(to_compress), "last diag", last.diag, "now diag", row.diag)
                last = row
                record_lists_for_compressing.append(to_compress)
                to_compress = [row, ]
                used_columns = set(non_empty_columns)
        if len(to_compress) > 0:
            if debug:
                print("Compressed last records per person")
            record_lists_for_compressing.append(to_compress)
            if debug:
                print("Connected ", len(to_compress), "records")


    compressed_records = []

    for record_list in record_lists_for_compressing:
        first = record_list[0]
        for row in record_list[1:]:
            non_empty_columns = []
            for column in feature_columns:
                if np.isfinite(row[column]):
                    non_empty_columns.append(column)
            for column in non_empty_columns:
                first[column] = row[column]

        compressed_records.append(first)

    compressed_frame = pandas.DataFrame.from_records(compressed_records, columns=columns)

    # validate:
    for feature in feature_columns:
        features_compressed = compressed_frame[feature]
        features_orig = frame[feature]
        features_orig_len = len(np.unique(features_orig[np.isfinite(features_orig)]))
        features_compressed_len = len(np.unique(features_compressed[np.isfinite(features_compressed)]))
        if features_compressed_len!=features_orig_len:
            pass

    return compressed_frame