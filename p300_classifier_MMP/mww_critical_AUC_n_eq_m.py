#!/usr/bin/python2
# coding: utf-8
import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib as mpl


# Make data.
N = np.arange(1, 151)

# poziom p (a właściwie alfa)
p = 0.05

U = np.loadtxt("mww-U-p{}-n=m-uppertail.dat".format(p))


AUC = U/N**2


