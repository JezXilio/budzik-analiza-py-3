#!/usr/bin/env python2
# coding: utf8
from classify_score import boot_strap_aucs
import numpy as np


from balancer import FirstNTooBigException

AVERAGE = 1

n_trials = np.arange(1, 30, 1)

for nr, i in enumerate(n_trials):
    try:
        boot_strap_aucs(average=i, first_n=None, output_prefix="/repo/fizmed/coma/results/mdovgialo/result_archive/wyniki_n_usrednien/wyniki_symulacje/downsampling/MP_r21_200_tgt", pipeline_type='downsampling')
    except FirstNTooBigException:
        print "FINISHED"
        break
    except (ValueError, UserWarning, RuntimeWarning) as e:
        print
        print
        print
        print "Skipping ", i, 'Error:',  e
        pass

