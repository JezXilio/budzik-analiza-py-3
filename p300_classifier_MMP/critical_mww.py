#!/usr/bin/python2                                                             
# coding: utf-8                                                                
import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt
import sys
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib as mpl
import pandas as pd

# Make data.
rn = np.arange(1, 61)
N1, N2 = np.meshgrid(rn, rn)

print N1
print N2

results = pd.read_csv(sys.argv[1])

n1 = np.array(results[results["pipeline_type"] == "clusterisation"]["n_targets"])
n2 = np.array(results[results["pipeline_type"] == "clusterisation"]["n_nontargets"])
auc95 = np.array(results[results["pipeline_type"] == "clusterisation"]["auc95"])

n1d = np.array(results[results["pipeline_type"] == "downsample"]["n_targets"])
n2d = np.array(results[results["pipeline_type"] == "downsample"]["n_nontargets"])
auc95d = np.array(results[results["pipeline_type"] == "downsample"]["auc95"])

# poziom p (a w\305\202a\305\233ciwie alfa)
p = 0.05

U = np.loadtxt("mww-U-p{}.dat".format(p))

AUC = 1 - U / (N1 * N2)

AUC_0 = np.zeros(np.array(AUC.shape) + 1)
AUC_0[1:, 1:] = AUC
AUC_0[0, :] = AUC_0[:, 0] = np.nan

fig = plt.figure()
im = plt.imshow(AUC_0, cmap=cm.gist_rainbow, vmin=np.nanmin(AUC) - 0.001, vmax
=1, interpolation="nearest", origin="lower")
cbar = fig.colorbar(im, shrink=0.5, aspect=5)
cbar.set_label('AUC')
plt.xlabel("n1")
plt.ylabel("n2")

AUC[N1 > N2] = np.nan
fig = plt.figure()
ax = fig.gca(projection='3d')

# special colormap
colors = [(0, 0, 0, 0)]
colors.extend(cm.gist_rainbow(np.linspace(0, 1, 1000)))
cmap = mpl.colors.ListedColormap(colors)
cmap.set_bad('black')

surf = ax.plot_surface(N1, N2, AUC, cmap=cmap, linewidth=0, antialiased=False,
                       vmin=np.nanmin(AUC) - 0.001, vmax=1, rstride=1, cstride=1)
ax.scatter(n1, n2, auc95, marker="*")
ax.scatter(n1d, n2d, auc95d, marker="+")

cbar = fig.colorbar(surf, shrink=0.5, aspect=5)
cbar.set_label('AUC')
ax.view_init(30, -45)

meanrank = rn ** 2 / 2
sd = np.sqrt(rn ** 2 * (2 * rn + 1) / 12.0)  # bez tiecorrection, ale powinno by\304\207 ok

z = st.norm.ppf(1 - p)  # jednostronnie
u = z * sd + meanrank + 0.5
print u

auc_2 = u / rn ** 2
auc_1 = AUC[rn - 1, rn - 1]

plt.figure()
plt.subplot(311)
plt.title(u"AUC krytyczny obliczony dwiema metodami: 1 - dok\305\202adna, 2 - przybli\305\274ona rozk. norm., p = 0.05")
plt.plot([0, 61], [1, 1], 'gray', linewidth=1)
plt.plot(rn, auc_1, label="AUC_1")
plt.scatter(n1, auc95, marker="*")
plt.scatter(n1d, auc95d, marker="+")
plt.plot(rn, auc_2, label="AUC_2")
plt.xlim(0, 61)
plt.ylabel("AUC")
plt.legend()

plt.subplot(312)
plt.plot([0, 61], [0, 0], 'gray', linewidth=1)
plt.plot(rn, (auc_1 - auc_2), label=u"r\303\263\305\274nica")
plt.xlim(0, 61)
plt.ylabel("AUC_1 - AUC_2")
plt.legend()

plt.subplot(313)
plt.plot([0, 61], [0, 0], 'gray', linewidth=1)
plt.plot(rn, (auc_1 - auc_2), label=u"r\303\263\305\274nica")
plt.ylim(-0.004, 0.004)
plt.xlim(0, 61)
plt.ylabel("AUC_1 - AUC_2")
plt.xlabel(u"n (r\303\263wnoliczne grupy)")
plt.legend()

plt.show()
