import sys
import os
import glob
from subprocess import check_call
import process_folder_pre


cwd = os.path.dirname(os.path.abspath(__file__))

folders = sys.argv[1:]
process_folder_pre.process_folder_pre(folders)
for folder in folders:
    for csv in glob.glob(os.path.join(folder, '_sorted', '*.csv')):
        check_call(['python2', os.path.join(cwd, 'draw_boxplots.py'), csv])
    check_call(['python2', os.path.join(cwd, 'cluster_hist.py'), os.path.join(folder, '_sorted')])
    check_call(['python2', os.path.join(cwd, 'draw_boxplots_max_val.py'), os.path.join(folder, '_sorted', 'results_database.csv')])
    check_call(['python2', os.path.join(cwd, 'draw_boxplots_max_val_m.py'), os.path.join(folder, '_sorted', 'results_database.csv')])


