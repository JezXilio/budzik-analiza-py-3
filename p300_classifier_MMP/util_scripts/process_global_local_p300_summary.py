import sys

from subprocess import check_call

import os
import numpy as np
import pandas as pd

from collections import OrderedDict

def write_roman(num):

    roman = OrderedDict()
    roman[1000] = "M"
    roman[900] = "CM"
    roman[500] = "D"
    roman[400] = "CD"
    roman[100] = "C"
    roman[90] = "XC"
    roman[50] = "L"
    roman[40] = "XL"
    roman[10] = "X"
    roman[9] = "IX"
    roman[5] = "V"
    roman[4] = "IV"
    roman[1] = "I"

    def roman_num(num):
        for r in roman.keys():
            x, y = divmod(num, r)
            yield roman[r] * x
            num -= (r * x)
            if num > 0:
                roman_num(num)
            else:
                break

    return "".join([a for a in roman_num(num)])

workdir = sys.argv[-1]

paradigms = 'global_local_global_early,global_local_global_late,global_local_local_early,global_local_local_late,p300'.split(',')

databases = []

for paradigm in paradigms:
    if not os.path.exists(os.path.join(workdir, paradigm, "average_n_1_first_n_None", "_sorted")):
        try:
            check_call(['python2',
                        os.path.join(os.path.dirname(__file__), "process_folder.py"),
                        os.path.join(workdir, paradigm, "average_n_1_first_n_None")
                        ]
                       )
        except:
            pass
    csv_path = os.path.join(workdir, paradigm, "average_n_1_first_n_None", "_sorted/", "results_database.csv")
    if 'global_local':
        check_call(['sed', '-i.back' , "s/global_local,/{},/".format(paradigm),
                    csv_path])
    database = pd.read_csv(csv_path)

    database = database[database.measure=="AUC"]
    databases.append(database)



result = pd.concat(databases)
resulting_csv_path = os.path.join(workdir, 'result_database_full.csv')
result.to_csv(resulting_csv_path)

check_call(['python3',
            os.path.join(os.path.dirname(__file__), '..', '..', 'AUC', 'add_statistical_significance_column.py'),
            resulting_csv_path]
           )

stat_sign = pd.read_csv(os.path.join(workdir, 'result_database_full_statistical_significance.csv'))



# import IPython
# IPython.embed()

patients = set(stat_sign[(stat_sign.diagnosis != 'CONTROL_ADULT') & (stat_sign.diagnosis != 'CONTROL_CHILDREN')].id)
database_paradigms = set(stat_sign.paradigm)
database_paradigms = "p300_sl,p300_sl_tony,p300_cz,p300_wz,global_local_global_early,global_local_global_late,global_local_local_early,global_local_local_late".split(',')
rounds = list(set(stat_sign['round']))
best_round_per_patient = {}
for patient in patients:
    round_numbers = []
    for round in rounds:
        round_numbers.append(len(stat_sign[np.logical_and(stat_sign['id'] == patient, stat_sign['round'] == round)]))
    best_round_per_patient[patient] = rounds[np.argmax(round_numbers)]

summary_csv = os.path.join(workdir, 'summary.csv')
header = ['id','diagnoza'] + ['runda'] + list(database_paradigms)
with open(summary_csv, 'w') as f:
    f.write(','.join(header))
    f.write('\n')
    for patient in sorted(patients):

        # import IPython
        # IPython.embed()
        diagnoza = stat_sign[(stat_sign['id'] == patient) & (stat_sign['round'] == best_round_per_patient[patient])]
        diagnoza = list(diagnoza.diagnosis)[0]
        for round in rounds:

            tempdata = stat_sign[(stat_sign['id'] == patient) & (stat_sign['round'] == round)]
            if len(tempdata) == 0:
                continue
            f.write('{},{},{},'.format(patient, diagnoza, write_roman(int(round)), ))
            for paradigm in list(database_paradigms):


                data = stat_sign[(stat_sign['id'] == patient) & (stat_sign['round'] == round) & (stat_sign['paradigm'] == paradigm)]
                if len(data)>0:
                    AUC = list(data['value'])[0]
                    sign05 = list(data['simulation0.05'])[0]
                    sign01 = list(data['simulation0.01'])[0]
                    f.write('{:0.3}'.format(AUC))
                    if sign01>0:
                        f.write('*')
                    if sign05>0:
                        f.write('*')
                    f.write(',')
                else:
                    f.write(',')
            f.write('\n')


