# coding: utf-8
import json
import numpy as np
import matplotlib
matplotlib.use("Agg")
import pylab as pb

from collections import defaultdict
import os
import re

import sys

PARADIGMS = {'p300_wz': 'p300_wz', 'p300_cz': 'p300_cz',
             'p300_sl': ['p300_sl', 'tony'],
             'p300_sl_tony': 'p300_sl.*tony'}


def get_groups_for_paradigm(paradigm):
    try:
        workfolder = sys.argv[1]
        print "Workfolder:", workfolder
    except IndexError:
        workfolder = '.'

    groups = [i.upper() for i in ['control_adult', 'control_children', 'emcs', 'mcs-', 'mcs+', 'uws']]

    cluster_timings = []

    p_values = []
    aucs = []

    for g in groups:
        items = os.listdir('{}/{}'.format(workfolder, g))

        for item in items:
            print item, paradigm, type(paradigm)
            if isinstance(paradigm[1], (list, tuple)):
                pos_re = paradigm[1][0]
                neg_re = paradigm[1][1]
                if re.search(pos_re, item.lower()) is None:
                    print 'SKIPPED'
                    continue
                if re.search(neg_re, item.lower()):
                    print 'SKIPPED'
                    continue
            else:
                if re.search(paradigm[1], item.lower()) is None:
                    print 'SKIPPED'
                    continue
            print 'Reading:', item
            try:
                clusters = np.load(os.path.join(workfolder, g, item, 'clusters.npy'))
                with open(os.path.join(workfolder, g, item, 'data.json')) as f:
                    data = json.load(f)
                    p_values.append(np.mean(data['cluster_p_values']))
                    aucs.append(data['auc'])
                    times_l = clusters.shape[2]
                    times = np.arange(0, times_l, 1.0 / data['fs']) + data['baseline']

                    for cluster_id in xrange(clusters.shape[0]):
                        for channel_id in xrange(clusters.shape[2]):
                            selected_times = list(times[clusters[cluster_id, :, channel_id]])
                            cluster_timings.extend(selected_times)
            except:
                import traceback
                traceback.print_exc()
                print "Skipping", workfolder, g, item

                continue

    pb.figure(figsize=(10, 10))
    pb.hist(cluster_timings, bins=100)
    pb.xlim([-.2, 1.5])
    pb.axvline(0)
    pb.title('Found cluster histogram')
    # print 'Cluster_timings: ', cluster_timings
    pb.savefig(os.path.join(workfolder, 'cluster_hist_{}.png'.format(paradigm[0])))

    pb.figure(figsize=(10, 10))
    pb.scatter(p_values, aucs)
    pb.xlabel('P values of selected cluster')
    pb.ylabel('AUC values')
    pb.xlim([0,1])
    pb.ylim([0, 1])

    # print 'p_values: ', p_values
    # print 'aucs: ', aucs

    pb.savefig(os.path.join(workfolder, 'cluster_p_vs_auc_{}.png'.format(paradigm[0])))


for paradigm in PARADIGMS.items():
    get_groups_for_paradigm(paradigm)





