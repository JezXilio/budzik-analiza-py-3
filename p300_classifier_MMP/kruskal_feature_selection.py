from sklearn.base import TransformerMixin
import scipy.stats as st
import numpy

draw = False

class KruskalSelector(TransformerMixin, object):

    def __repr__(self):
        return "<KruskalWallis Selector>"

    def fit(self, features, classes):
        print "KRUSKAL GOT {} features".format(features.shape[1])
        features_to_use = []
        for feature in range(features.shape[1]):
            to_test = []
            for cls in numpy.unique(classes):
                selected = features[classes==cls][:, feature]
                to_test.append(selected)
            # print to_test[0].shape, to_test[1].shape
            try:
                Hvalue, pvalue = st.kruskal(*to_test)
            except ValueError:
                continue
            # print len(selected)
            # print 'CLASES', numpy.unique(classes)
            # print pvalue
            if pvalue < 0.2:
                features_to_use.append(feature)
        self.features_to_use = features_to_use
        if draw:
            import pylab
            for i in features_to_use:

                f = pylab.figure()
                ax = f.add_subplot(111)
                ax.hist(features[classes==0][:, i], color='r', alpha=0.5)
                ax.hist(features[classes==1][:, i], color='b', alpha=0.5)
                f.savefig('feature_dist_{}.png'.format(i))
                pylab.close(f)

        return self

    def transform(self, features):
        if self.features_to_use:
            print "KRUSKAL FOUND {} usefull features".format(len(self.features_to_use))
            return features[:, self.features_to_use]
        else:
            print "WARNING, Kruskall found no usefull features, using all {}".format(len(self.features_to_use))
            return features
