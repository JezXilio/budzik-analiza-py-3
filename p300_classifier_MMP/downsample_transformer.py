import mne
from sklearn.base import TransformerMixin
import scipy.signal as ss
import numpy
import numpy as np

class DownsampleTransformer(TransformerMixin, object):

    def __init__(self, Fs=128.0, bas=-0.1,
                                 window=1.1,
                                 targetFs=24, flatten = True):
        self.Fs = Fs
        self.bas = bas
        self.window = window
        self.targeFs = targetFs
        self.flatten = flatten

    def fit(self, features, classes):
        return self

    def transform(self, features):

        if not isinstance(features, np.ndarray):
            features = features.get_data()
        new_features = []
        for epoch in features:
            down = _feature_extraction_singular(epoch, Fs=self.Fs, bas=self.bas, window=self.window, targetFs=self.targeFs)
            #print "downsampled", down.shape
            if self.flatten:
                down = down.flatten()
            new_features.append(down)
        return numpy.array(new_features)

    def __repr__(self):
        return "<DownsampleTransformer fs={}>".format(self.targeFs)


def gcd(*numbers):
    """Return the greatest common divisor of the given integers"""
    from fractions import gcd
    return reduce(gcd, numbers)

# Least common multiple is not in standard libraries? It's in gmpy, but this is simple enough:

def lcm(*numbers):
    """Return lowest common multiple."""
    def lcm(a, b):
        return (a * b) // gcd(a, b)
    return reduce(lcm, numbers, 1)

def _feature_extraction_singular(epoch, Fs, bas=-0.1,
                                 window=0.5,
                                 targetFs=30, ):
    '''performs feature extraction on epoch (array channels x time),
    Args:
        Fs: sampling in Hz
        bas: baseline in seconds
        targetFs: target sampling in Hz (will be approximated)
        window: timewindow after baseline to select in seconds or list - [positive seconds after baseline start, positive seconds after baseline stop]

    Returns: 1D array downsampled, len = downsampled samples x channels
    epoch minus mean of baseline, downsampled by factor int(Fs/targetFs)
    samples used - from end of baseline to window timepoint
     '''
    #print "EPOCH", epoch.shape
    mean = np.mean(epoch[:, :int(-bas * Fs)], axis=1)

    ratio = Fs/targetFs

    ratio_lcm = lcm(int(Fs), int(targetFs))

    up = ratio_lcm
    down = ratio * ratio_lcm

    epoch_baseline_corrected = epoch - mean[:, None]

    epoch_without_baseline = epoch_baseline_corrected[:, int(-bas * Fs):]

    if isinstance(window, list):
        window_start = window[0]
        window_stop = window[1]
    else:
        window_start = 0
        window_stop = window

    selected = epoch_without_baseline[:, int(window_start * Fs):int(window_stop * Fs)]

    features = ss.resample_poly(selected, up, down, axis=1, )
    return features.flatten()


