import collections
import numpy
import scipy.signal
from .io import Signal


PowerData = collections.namedtuple('PowerData', ('arr_f', 'arr_t', 'values'))


class NoValidEpochsException(Exception):
    def __init__(self):
        super().__init__('no valid epochs')


class PowerExtractor:
    def __init__(self,
                 t_step: float = 0.5, # seconds
                 f_step: float = 0.5, # Hz
                 use_welch: bool = False,
                 window: str = 'hanning',
                 artifact_method = None):
        self.t_step = t_step/2 if use_welch else t_step
        self.f_step = f_step
        self.use_welch = use_welch
        self.window = window
        self.artifact_method = artifact_method

    def extract(self, signal: Signal, montage: dict, tag_names: str, path_to_tag: str, t_min: float, t_max: float, f_max: float):
        channel = signal.montage(montage)
        if self.artifact_method is not None:
            channel = self.artifact_method.replace_artifacts(channel)
        all_labeled_epochs = channel.get_epochs(path_to_tag, t_min, t_max)
        epochs = [labeled_epoch.epoch for labeled_epoch in all_labeled_epochs if labeled_epoch.label in tag_names]
        return self.calculate(epochs, t_min, f_max)

    def calculate(self, epochs: list, t_min: float, f_max: float):
        if not epochs:
            raise NoValidEpochsException()

        maps = []
        for epoch in epochs:
            window_length = round(2 * self.t_step * epoch.freq_sampling)
            noverlap = round(self.t_step * epoch.freq_sampling)
            nfft = round(epoch.freq_sampling / self.f_step)

            arr_f, arr_t, fft = scipy.signal.spectrogram(epoch.data, epoch.freq_sampling,
                                                                 nperseg=window_length, noverlap=noverlap, nfft=nfft,
                                                                 window=self.window)
            bins = numpy.sum(arr_f <= f_max)
            fft = fft[0:bins]
            if self.use_welch:
                fft = scipy.signal.convolve(fft, numpy.ones((1,3)), mode='valid')
                arr_t = arr_t[1:-1]
            maps.append(fft)

        arr_t += t_min
        arr_f = arr_f[0:bins]
        spec_data = numpy.zeros([len(maps), len(arr_f), len(arr_t)])
        for index in range(len(maps)):
            spec_data[index] = maps[index]
        return PowerData(arr_f, arr_t, spec_data)
