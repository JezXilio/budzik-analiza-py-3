#!/usr/bin/env python3
import sys
import numpy
import pylab
import erds_helpers as erds

################################################################################

PAGE_LENGTH_SECONDS = 20

################################################################################

if len(sys.argv) < 2:
    sys.exit("USAGE: {} path-to-signal.obci.raw ...".format(sys.argv[0]))

try:
    for path_to_raw in sys.argv[1:]:
        if path_to_raw[-9:] != '.obci.raw':
            raise Exception("invalid path")
        core_of_path = path_to_raw[:-9]
        path_to_xml = core_of_path + '.obci.xml'
        signal = erds.get_preprocessed_signal(path_to_raw, path_to_xml)

        for fun_label, montage in erds.montages():
            raw_channel = signal.montage(montage)
            channel = erds.replace_artifacts_with_nan(raw_channel)
            page_length = PAGE_LENGTH_SECONDS * signal.freq_sampling
            t = numpy.arange(page_length) / signal.freq_sampling
            for i in range(0, signal.sample_count - page_length + 1, page_length):

                pylab.subplot(2, 1, 1)
                pylab.xlim(0, PAGE_LENGTH_SECONDS)
                pylab.ylim(-15, +15)
                pylab.plot(t, raw_channel.data[i:i+page_length])

                pylab.subplot(2, 1, 2)
                pylab.xlim(0, PAGE_LENGTH_SECONDS)
                pylab.ylim(-15, +15)
                pylab.plot(t, channel.data[i:i+page_length])

                pylab.show()

except Exception as e:
    # import traceback
    # traceback.print_exc()
    sys.exit('ERROR: {}'.format(e))
