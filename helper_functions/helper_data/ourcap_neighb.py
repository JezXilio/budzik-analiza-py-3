import mne
import scipy
import numpy


conn_mat = numpy.array([[bool(int(j)) for j in i.split(' ')] for i in """
1 1 1 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
1 1 1 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 1 0 1 1 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0
0 1 0 1 1 1 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0
0 1 1 0 1 1 1 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0
0 0 1 0 0 1 1 1 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0
0 0 1 0 0 0 1 1 0 0 0 0 1 1 1 0 0 0 0 0 0 0 0
0 0 0 1 0 0 0 0 1 1 0 0 0 0 0 1 0 0 0 0 0 0 0
0 0 0 1 1 0 0 0 1 1 1 0 0 0 0 1 1 0 0 0 0 0 0
0 0 0 1 1 1 0 0 0 1 1 1 0 0 0 1 1 1 0 0 0 0 0
0 0 0 0 1 1 1 0 0 0 1 1 1 0 0 0 1 1 1 0 0 0 0
0 0 0 0 0 1 1 1 0 0 0 1 1 1 0 0 0 1 1 1 0 0 0
0 0 0 0 0 0 1 1 0 0 0 0 1 1 1 0 0 0 1 1 0 0 0
0 0 0 0 0 0 0 1 0 0 0 0 0 1 1 0 0 0 0 1 0 0 0
0 0 0 0 0 0 0 0 1 1 1 0 0 0 0 1 1 0 0 0 1 0 0
0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 1 1 1 0 0 1 1 0
0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 1 1 1 0 1 1 1
0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 1 1 1 0 1 1
0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 1 1 0 0 1
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 1 1 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 1 1 1
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 1 1
""".strip().split('\n')])

conn_mat_sparse = numpy.array([[bool(int(j)) for j in i.split(' ')] for i in """
1 1 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
1 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
1 1 1 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 1 1 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0
0 1 0 1 1 1 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0
1 0 0 0 1 1 1 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0
0 0 1 0 0 1 1 1 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 1 1 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 1 0 0 0 0 1 1 1 0 0 0 0 1 0 0 0 0 0 0 0
0 0 0 0 1 0 0 0 0 1 1 1 0 0 0 0 1 0 0 0 0 0 0
0 0 0 0 0 1 0 0 0 0 1 1 1 0 0 0 0 1 0 0 0 0 0
0 0 0 0 0 0 1 0 0 0 0 1 1 1 0 0 0 0 1 0 0 0 0
0 0 0 0 0 0 0 1 0 0 0 0 1 1 1 0 0 0 0 1 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 1 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 1 1 1 0 0 1 0 0
0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 1 1 1 0 0 1 0
0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 1 1 1 0 0 1
0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 1 1 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 1 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 1
""".strip().split('\n')])


connectivity_channels = 'Fpz,Fp1,Fp2,F7,F3,Fz,F4,F8,M1,T3,C3,Cz,C4,T4,M2,T5,P3,Pz,P4,T6,O1,Oz,O2'.split(',')

connectity_matrix = scipy.sparse.csr.csr_matrix(conn_mat)
connectity_matrix_sparse = scipy.sparse.csr.csr_matrix(conn_mat_sparse)

def get_our_connectivity(channels):
    """Returns connectivity matrix for cap used in budzik"""
    picks = [connectivity_channels.index(i) for i in channels]

    # picking before constructing matrix is buggy
    connectivity = connectity_matrix[picks][:, picks]
    ch_names = [connectivity_channels[p] for p in picks]
    return scipy.sparse.csr.csr_matrix(connectivity)


def get_our_connectivity_sparse(channels):
    """Returns connectivity matrix for cap used in budzik"""
    picks = [connectivity_channels.index(i) for i in channels]

    # picking before constructing matrix is buggy
    connectivity = connectity_matrix_sparse[picks][:, picks]
    ch_names = [connectivity_channels[p] for p in picks]
    return scipy.sparse.csr.csr_matrix(connectivity)


def show_connectivity(connectivity, chnames):
    mne.viz.plot_connectivity_circle(connectivity.toarray() + 0.1, chnames)