#coding: utf8


import matplotlib
matplotlib.use('Agg')
import pylab as pb

import sys
import traceback
import os.path, os
import numpy as np
from matplotlib.axes._base import _AxesBase

from helper_functions import (evoked_list_plot_smart_tags, get_epochs_fromfile_flist,
                              stag_amp_grad_art_rej, savetags,
                              standard_preprocessing)
from obci.analysis.obci_signal_processing.tags import tags_file_writer as tags_writer
from obci.analysis.obci_signal_processing.tags import tag_utils
from analyse_common import OUTDIR, get_filelist, get_clean_epochs
from scipy.signal import welch



def get_tagfilters(blok_type):
    if blok_type==1:
        def target_func(tag):
            if tag['desc']['blok_type']=='1' and tag['desc']['type']=='target':
                return True
            else:
                return False
        def nontarget_func(tag):
            if tag['desc']['blok_type']=='1' and tag['desc']['type']=='nontarget':
                return True
            else:
                return False
    elif blok_type==2:
        def target_func(tag):
            if tag['desc']['blok_type']=='2' and tag['desc']['type']=='target':
                return True
            else:
                return False
        def nontarget_func(tag):
            if tag['desc']['blok_type']=='2' and tag['desc']['type']=='nontarget':
                return True
            else:
                return False
    return target_func, nontarget_func


        


def spectrum(filename, *args):
    '''analyzes P300cz as of design document'''
    montages = [['csa'] ,['custom', 'M1', 'M2'], ['custom', 'Cz']]
    filtr = [[2, 30], [0.1, 35], 3, 12]
    filtr = None
    drop_chnls =  [u'l_reka', u'p_reka', u'l_noga', u'p_noga', u'haptic1', u'haptic2',
                    u'phones', u'Driver_Saw']

    epoch_labels = ['target', 'nontarget']
    channels_to_draw = 'F3;F4;C3;C4;P3;P4;T3;T4;O1;O2;Fz;Pz;Cz'.split(';')
    filelist, filename = get_filelist(filename, args)

    for montage in montages:
        for filename in filelist:
            if 'etr' in filename:
                continue
            print 'working on {}'.format(filename[:-4])
            try:
                rm = standard_preprocessing(filename[:-4],
                                            drop_chnls=drop_chnls,
                                            montage=montage,
                                            filter=filtr,
                                            )
            except Exception as e:
                print e
                continue
            fs = float(rm.get_param('sampling_frequency'))
            data = rm.get_channels_samples(channels_to_draw)
            f, Ps = welch(data, fs=fs, nperseg=2**12, detrend='linear')
            for logarithm in [True, False]:
                ncols = 4
                nrows = len(channels_to_draw)/4+1
                fig, axes = pb.subplots(nrows=nrows, ncols=ncols, squeeze=False,
                                        figsize=(5*ncols, 3*nrows))
                for nr, chnl in enumerate(channels_to_draw):
                    P = Ps[nr,:]
                    if logarithm:
                        mask = np.logical_and(f>=0, f<=40)
                    else:
                        mask = np.logical_and(f>=1, f<=40)
                    ax = axes.flatten()[nr]
                    args = f[mask], P[mask]
                    if logarithm:
                        try:
                            ax.semilogy(*args)
                        except Exception:
                            pass
                    else:
                        ax.plot(*args)
                    ax.set_title(chnl)
                    #ax.set_xlim(0, 40)
                    ax.set_ylabel('Power density [uV**2/Hz]')
                    ax.set_xlabel('Freq, [Hz]')
                    ax.grid('on',)






                suffix = '_{}_spectrum_welch_log+{}'.format(montage, logarithm)
                savefileimg = os.path.join(OUTDIR+'/spect_real/', os.path.basename(filename)+suffix+'.png')
                fig.savefig(savefileimg)
                pb.close(fig)




def main():
    try:
        filename = sys.argv[1:]
    except IndexError:
        raise IOError( 'Please provide path to .raw')
    spectrum(*filename)

if __name__ == '__main__':
    main()

